<?php
/**
 * Created by PhpStorm.
 * User: evgeniy
 * Date: 24.07.18
 * Time: 14:27
 */

namespace MfoRu\MfoAccounting\Bistrodengi;


class DebugData implements \MfoRu\Contracts\MfoAccounting\DebugData
{
    protected $requestHeader;
    protected $requestBody;

    protected $responseHeader;
    protected $responseBody;

    function __construct($debugArr)
    {
        if(isset($debugArr['response_header']))
            $this->responseHeader = $debugArr['response_header'];

        if(isset($debugArr['response_body']))
            $this->responseBody = $debugArr['response_body'];

        if(isset($debugArr['request_header']))
            $this->requestHeader = $debugArr['request_header'];

        if(isset($debugArr['request_body']))
            $this->requestBody = $debugArr['request_body'];
    }

    function getRequestBody(): string
    {
        return $this->requestBody;
    }

    function getRequestHeader(): string
    {
        return $this->requestHeader;
    }

    function getResponseBody(): string
    {
        return $this->responseBody;
    }

    function getResponseHeader(): string
    {
        return $this->responseHeader;
    }
}