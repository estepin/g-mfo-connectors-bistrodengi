<?php
namespace MfoRu\MfoAccounting\Bistrodengi;


use MfoRu\Contracts\MfoAccounting\Anket;

class Formatter
{
    function anketToApiArray(Anket $anket, $token, $isTest = false)
    {
        if($anket->gender == 1)
            $gender = 'male';
        elseif ($anket->gender == 2)
            $gender = 'female';

        $birthdate = date('d.m.Y', strtotime($anket->birthdate));
        $exP = explode(' ', $anket->passSeriaNum);
        $passSeriaNum = sprintf('%s%s %s', $exP[0], $exP[1], $exP[2]);

        $arr = [
            'firstname' => $anket->firstname,
            'lastname' => $anket->lastname,
            'middlename' => $anket->middlename,
            'mobilephone' => $this->formatPhone($anket),
            'birthdate' => $birthdate,
            'passport_code' => $passSeriaNum,
            'birthplace' => $anket->passBirthPlace,
            'email' => $anket->email,
            'gender' => $gender,

            'activity' => 'Работа по найму',

            'credit_days' => $anket->term,
            'credit_summ' => $anket->summ,
            'fact_region_name' => $anket->liveRegion,
            'fact_city_name' => $anket->liveCity,
            'fact_city_street' => $anket->liveStreet,
            'fact_house' => $anket->liveHouse,
            'fact_housing' => $anket->liveBuild,
            'fact_flat' => $anket->liveRoom,

            'reg_region_name' => $anket->regRegion,
            'reg_city_name' => $anket->regCity,
            'reg_city_street' => $anket->regStreet,
            'reg_house' => $anket->regHouse,
            'reg_housing' => $anket->regBuild,
            'reg_flat' => $anket->regRoom,

            'accessToken' => $token
        ];

        if($isTest)
            $arr['is_test'] = true;

        return $arr;
    }

    function formatPhone(Anket $anket)
    {
        if(preg_match( '/^\+(\d)(\d{3})(\d{7})$/', $anket->phone,  $matches))
        {
            $result = sprintf('%s (%s) %s', $matches[1], $matches[2], $matches[3]);
            return $result;
        }
        else
        {
            throw new \Exception('Wrong phone');
        }
    }
}