<?php
namespace MfoRu\MfoAccounting\Bistrodengi;


use MfoRu\Contracts\MfoAccounting\Anket;
use MfoRu\Contracts\MfoAccounting\Exceptions\DuplicateRequest;
use MfoRu\Contracts\MfoAccounting\Exceptions\NotAuth;
use MfoRu\Contracts\MfoAccounting\Exceptions\UnknownResponse;
use MfoRu\MfoAccounting\Bistrodengi\DebugData;
use MfoRu\Contracts\MfoAccounting\DebugData as  InterfaceDebugData;

class Connector implements \MfoRu\Contracts\MfoAccounting\Connector
{
    protected $debugData;

    function addToUpload(Anket $anket, $config)
    {
        return $this->sendRequest($anket, $config, false);
    }

    protected function sendRequest(Anket $anket, $config, $isTest)
    {
        $formatter = new Formatter();
        $apiClient = new Api();

        $arrAnket = $formatter->anketToApiArray($anket, $config->token, $isTest);
        $result = $apiClient->sendRequest($arrAnket);
        $this->debugData = new DebugData($apiClient->getDebugData());

        if(isset($result->status) && $result->status == 'success')
        {
            return $result->id;
        }
        elseif($result->message == 'Ошибка доступа. Не задан или задан неправильный токен')
        {
            throw new NotAuth($result->message);
        }
        elseif ($result->message == 'Ошибка входных данных' && ($result->errors->passport_code == 'Дублирующая заявка' || $result->errors->mobilephone == 'Клиент с таким номером телефона уже оставлял заявку'))
        {
            throw new DuplicateRequest();
        }
        else
        {
            throw new UnknownResponse();
        }
    }

    function getAccData($id)
    {
        // TODO: Implement getAccData() method.
    }

    function getConfigModel()
    {
        return new Config();
    }

    function checkStatus($anketId, $config)
    {
        // TODO: Implement checkStatus() method.
    }

    function getDebugData():InterfaceDebugData
    {
        return $this->debugData;
    }

    function testConfig($config): bool
    {
        $anket = $this->getTestAnket();
        $result = $this->sendRequest($anket, $config, true);

        return (bool) $result;
    }

    function getTestAnket()
    {
        $anket = new Anket();

        $anket->summ = '1000';
        $anket->term = '10';

        $anket->lastname = 'Петров';
        $anket->firstname = 'Петр';
        $anket->middlename = 'Петрович';
        $anket->gender = '1';
        $anket->birthdate = '1990-01-01';

        $anket->email = 'test@mfo.ru';
        $anket->phone = '+79091112233';

        $anket->passSeriaNum = '11 26 123321';
        $anket->passWhoIssued = 'УВД';
        $anket->passCode = '123-321';
        $anket->passDate = '2011-01-01';
        $anket->passBirthPlace = 'Саратов';

        $anket->regIndex = '410001';
        $anket->regRegion = 'Саратовская обл';
        $anket->regCity = 'Саратов';
        $anket->regStreet = 'Московская';
        $anket->regHouse = '1';
        $anket->regRoom = 1;

        $anket->liveIndex = '410001';
        $anket->liveRegion = 'Саратовская обл';
        $anket->liveCity = 'Саратов';
        $anket->liveStreet = 'Московская';
        $anket->liveHouse = '1';
        $anket->liveRoom = 1;

        return $anket;
    }
}