<?php
namespace MfoRu\MfoAccounting\Bistrodengi;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use MfoRu\Contracts\MfoAccounting\Exceptions\ServerUnavailable;

class Api
{
    protected $client;
    protected $formatter;
    protected $debugData;

    function __construct()
    {
        $this->client = new Client();
    }

    function sendRequest($arRequest)
    {
        $client = new Client();
        try {
            $res = $client->post(
                'https://bistrodengi.ru/api/addloan/',
                [
                    'form_params' => $arRequest
                ]
            );
        }catch(ConnectException $e) {
            throw new ServerUnavailable($e->getMessage());
        }

        $body = $res->getBody()->getContents();
        $this->debugData['response_body'] = $body;
        $this->debugData['response_header'] = $this->headersToString($res->getHeaders());
        $this->debugData['request_body'] = http_build_query($arRequest);
        return \GuzzleHttp\json_decode($body);
    }

    function getDebugData()
    {
        return $this->debugData;
    }

    protected function headersToString($array)
    {
        $header = '';
        foreach ($array as $name => $value)
        {
            foreach($value as $v)
            {
                $header .= $name.': '.$v."\n";
            }
        }

        return $header;
    }
}